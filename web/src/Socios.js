import React, { useState, useEffect } from "react";
import styled from 'styled-components';
import { socket } from "./ws";

const Container = styled.div`
    width: 300px;
    max-width: 300px;
`
const ContainerBody = styled.div` 
    height: 350px;
    overflow: scroll;
`
const Socios = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 15px;
    position: relative;
`
const Body = styled.div`
    padding-left: 15%;
    padding-right: 15%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`
const Name = styled.p`
    color: #333;
`
const Phone = styled.p`
    
`
const Email = styled.p`

`
const Enable = styled.div`
    width: 35px;
    height: 35px;
    border-radius: 50%;
    cursor: pointer;

    background-color: ${props => props.enable ? 'green' : 'red'};
    opacity: 0.5;
`
const Button = styled.button`
    background-color: red;
    color: white;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    opacity: 0.8;
    border-radius: 15px;
    width: 100%;
    height: 50px;
    margin-bottom: 150px;
`
const Icon = styled.img`
    width: 40px;
    height: 40px;
    cursor: pointer;
`
const FeedBack = styled.div`
    display: flex;
    flex-direction: row;
`

const App = () => {

    const [data, setData] = useState([]);

    useEffect(() => {

        socket.on('res:socios:view', ({ statusCode, data, message }) => {

            console.log('res:socios:view', { statusCode, data, message });

            console.log({ statusCode, data, message });

            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit('req:socios:view', ({})), 1000);

    }, []);

    const handleCreate = () => socket.emit('req:socios:create', {
        name: "Emigdio Uliambre",
        phone: "59521652513"
    });

    const handleDelete = (id) => socket.emit('req:socios:delete', { id });

    const handleChange = (id, status) => {
        if (status) socket.emit('req:socios:disable', { id })
        else socket.emit('req:socios:enable', { id });
    }

    return (

        <Container>
            <Button onClick={handleCreate}>Crear Socio</Button>
            <ContainerBody>

                {
                    data.map((v, i) => (
                        <Socios>

                            <Body>

                                <Name> {v.name} <caption>Socio Nro:{v.id}</caption> </Name>

                                <Phone> {v.phone} </Phone>

                            </Body>

                            <Body>

                                <Email> {v.email} </Email>

                                <FeedBack>
                                    <Enable enable={v.enable} onClick={() => handleChange(v.id, v.enable)} />
                                    <Icon src={"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Oxygen480-actions-edit-delete.svg/1200px-Oxygen480-actions-edit-delete.svg.png"}
                                        onClick={() => handleDelete(v.id)} />
                                </FeedBack>

                            </Body>

                        </Socios>
                    ))
                }
            </ContainerBody>
        </Container>
    )
}

export default App;