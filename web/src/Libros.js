import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { socket } from "./ws";

const Container = styled.div`
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: stretch;
    display: flex;
    width: 90%;
`
const Libro = styled.div`
    flex-direction: column;
    background-color: #303536;
    margin-bottom: 15px;
    border-radius: 10px;
    padding: 2px;
    display: flex;
    position: relative;
`
const Portada = styled.img`
    width: 200px;
`
const Icon = styled.img`
    width: 40px;
    height: 40px;
    position: absolute;
    top: -15px;
    right: -15px;
    cursor: pointer;
`

const Title = styled.p`
    color: white;
    text-align: center;
`
const Button = styled.button`
    background-color: #036e03;
    color: white;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    opacity: 0.8;
    border-radius: 15px;
    width: 100%;
    height: 50px;
    margin-bottom: 150px;
`

const Item = ({ image, title, id }) => {

    const handleDelete = () => socket.emit('req:libros:delete', { id });

    const opts = {
        src: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Oxygen480-actions-edit-delete.svg/1200px-Oxygen480-actions-edit-delete.svg.png",
        onClick: handleDelete
    }

    return (
        <Libro>
            <Icon {...opts} />
            <Portada src={image} />
            <Title>{title}</Title>
        </Libro>
    )
}

const App = () => {
    const [data, setData] = useState([]);

    useEffect(() => {

        socket.on('res:libros:view', ({ statusCode, data, message }) => {

            console.log('res:libros:view', { statusCode, data, message });

            console.log({ statusCode, data, message });

            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit('req:libros:view', ({})), 1000);

    }, []);

    const handleCreate = () => socket.emit('req:libros:create', {
        title: "Aprende NODEJS",
        image: "https://victorroblesweb.es/wp-content/uploads/2018/01/nodejs-victorroblesweb.png"
    });

    return (
        <Container>
            
            <Button onClick={handleCreate}>Crear Libro</Button>
            
            {data.map((v, i) => <Item {...v} />)}

        </Container>
    )
}

export default App