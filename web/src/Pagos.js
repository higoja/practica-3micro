import React, { useState, useEffect } from "react";
import styled from 'styled-components';
import { socket } from "./ws";

const Container = styled.div`
    width: 300px;
    max-width: 300px;
`
const ContainerBody = styled.div` 
    height: 350px;
    overflow: scroll;
`
const Pagos = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 15px;
    position: relative;
`
const Body = styled.div`
    padding-left: 15%;
    padding-right: 15%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`
const Name = styled.p`
    color: #333;
`
const Button = styled.button`
    background-color: red;
    color: white;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    opacity: 0.8;
    border-radius: 15px;
    width: 100%;
    height: 50px;
    margin-bottom: 150px;
`
const Icon = styled.img`
    width: 40px;
    height: 40px;
    cursor: pointer;
`
const Input = styled.input`
    margin-left: 10px;
    width: 90%;
    border-radius: 5px;
    height: 40px;
    cursor: pointer;
`

const App = () => {

    const [data, setData] = useState([]);
    const [value, setValue] = useState();

    useEffect(() => {

        socket.on('res:pagos:view', ({ statusCode, data, message }) => {

            console.log('res:pagos:view', { statusCode, data, message });

            console.log({ statusCode, data, message });

            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit('req:pagos:view', ({})), 1000);

    }, []);

    const handleCreate = () => value > 0 ? socket.emit('req:pagos:create', { socio: value, amount: 300 }) : null;

    const handleDelete = (id) => socket.emit('req:pagos:delete', { id });

    const handleInput = (e) => {
        setValue(e.target.value);
    }
    return (

        <Container>

            <Input type={'number'} onChange={handleInput}></Input>

            <Button onClick={handleCreate}>Aplicar Pago al Socio {value}</Button>

            <ContainerBody>

                {
                    data.map((v, i) => (
                        <Pagos>

                            <Body>

                                <Name>Cupon de pago : {v.id}</Name>

                                <Name>Socio : {v.socio} </Name>

                            </Body>

                            <Body>

                                <Name>{v.createdAd}</Name>

                                <Icon src={"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Oxygen480-actions-edit-delete.svg/1200px-Oxygen480-actions-edit-delete.svg.png"}
                                    onClick={() => handleDelete(v.id)} />

                            </Body>

                        </Pagos>
                    ))
                }
            </ContainerBody>
        </Container>

    )
};

export default App;