import React, { useEffect, useState } from "react";

import {socket} from './ws';

const App = () => {

    const [id, setId] = useState();

    useEffect(() => setTimeout(() => setId(socket.id), 500), []);

    setInterval(() => socket.emit('req:libros:view', ({})), 1000);


    return (
        <p>{id ? `On Line ${id}` : `Off Line ${id}`}</p>
    )
}
export default App;