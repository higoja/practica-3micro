const bull = require('bull');

const nameApi = require('./package.json');

const name = nameApi.name.replace('api-', '');

//console.log(nameApi.name.name);
//console.log(name);

/*
const { redis, name } = require('../microservice/src/setting');
No es viable ya que al empaquetar no se encotrara siempre con la misma ruta
al utilizarlo en otros micro
*/

const redis = { host: 'localhost', port: 6379 };

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull(`${name}:create`, opts);
const queueDelete = bull(`${name}:delete`, opts);
const queueUpdate = bull(`${name}:update`, opts);
const queueFindOne = bull(`${name}:findOne`, opts);
const queueView = bull(`${name}:view`, opts);

async function Create({ title, image }) {

    try {
        const job = await queueCreate.add({ title, image });

        console.log(nameApi.name + ':se agrega cola para CREATE con identificador ' + job.id);

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Delete({ id }) {

    try {
        const job = await queueDelete.add({ id });

        console.log(nameApi.name + ':se agrega cola para DELETE con identificador ' + job.id);

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Update({ id, title, category, seccions, image }) {

    try {
        const job = await queueUpdate.add({ id, title, category, seccions, image });

        console.log(nameApi.name + ':se agrega cola para UPDATE con identificador ' + job.id);

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function FindOne({ id }) {

    try {
        const job = await queueFindOne.add({ id });

        console.log(nameApi.name + ':se agrega cola para FINDONE con identificador ' + job.id);

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function View({ }) {

    try {
        const job = await queueView.add({});

        console.log(nameApi.name + ':se agrega cola para VIEW con identificador ' + job.id);

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

console.log(nameApi.name + ' listo...');

module.exports = { Create, Delete, Update, FindOne, View };