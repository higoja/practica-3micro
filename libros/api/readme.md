# API DE LIBROS

Api para interactuar con los libros de la biblioteca.

# EJEMPLO DE USO

```js
const apiLibros = require('api-libros');

io.on('connection', socket => {
    console.log("New connection", socket.id );

    /*
    ENDPOINT
    */

    // libros

    socket.on('req:libros:create', async ({ title }) => {

        try {

            console.log('req:libros:create', ({ title }));
            
            const { statusCode, data, message } = await apiLibros.Create({ title });

            io.to(socket.id).emit('res:libros:create', { statusCode, data, message });
            
        } catch (error) {

            console.log(error);
            
        }
    });

    socket.on('req:libros:delete', async ({ id }) => {

        try {

            console.log('req:libros:delete', ({ id }));
            
            const { statusCode, data, message } = await apiLibros.Delete({ id });

            io.to(socket.id).emit('res:libros:delete', { statusCode, data, message });
            
        } catch (error) {

            console.log(error);
            
        }
    });

    socket.on('req:libros:update', async ({ id, title, category, seccions }) => {

        try {

            console.log('req:libros:update', ({ id, title, category, seccions }));
            
            const { statusCode, data, message } = await apiLibros.Update({ id, title, category, seccions });

            io.to(socket.id).emit('res:libros:update', { statusCode, data, message });
            
        } catch (error) {

            console.log(error);
            
        }
    });

    socket.on('req:libros:findOne', async ({ id }) => {

        try {

            console.log('req:libros:findOne', ({ id }));
            
            const { statusCode, data, message } = await apiLibros.FindOne({ id });

            io.to(socket.id).emit('res:libros:findOne', { statusCode, data, message });
            
        } catch (error) {

            console.log(error);
            
        }
    });

    socket.on('req:libros:view', async ({ }) => {

        try {

            console.log('req:libros:view', ({ }));
            
            const { statusCode, data, message } = await apiLibros.View({ });

            io.to(socket.id).emit('res:libros:view', { statusCode, data, message });
            
        } catch (error) {

            console.log(error);
            
        }
    });
```