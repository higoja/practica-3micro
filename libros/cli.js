const { title } = require('process');
const apiLibros = require('./api');

async function main() {

    try {
        
        let { statusCode, data, message } = await apiLibros.Create({
            title: "Aprende NODEJS",
            image: "https://victorroblesweb.es/wp-content/uploads/2018/01/nodejs-victorroblesweb.png"
        })
        
        /*
        let { statusCode, data, message } = await apiLibros.Update({
            id: 4,
            title: "Aprende NODEJS",
            image: "https://victorroblesweb.es/wp-content/uploads/2018/01/nodejs-victorroblesweb.png"
        })
        */
        //let { statusCode, data, message } = await apiLibros.View({})

        console.log({ statusCode, data, message });

    } catch (error) {
        console.error(error)
    }

}

main()