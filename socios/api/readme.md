## API DE SOCIOS

Api para interactuar con los socios de la biblioteca.

## EJEMPLO DE USO

```js
const apiSocios = require('api-socios');

io.on('connection', socket => {
    socket.on('req:socios:create', async ({ name, phone }) => {

    try {

        console.log('req:socios:create', ({ name, phone }));
        
        const { statusCode, data, message } = await apiSocios.Create({ name, phone });

        io.to(socket.id).emit('res:socios:create', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});

socket.on('req:socios:delete', async ({ id }) => {

    try {

        console.log('req:socios:delete', ({ id }));
        
        const { statusCode, data, message } = await apiSocios.Delete({ id });

        io.to(socket.id).emit('res:socios:delete', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});

socket.on('req:socios:update', async ({ id, name, age, email, phone }) => {

    try {

        console.log('req:socios:update', ({ id, name, age, email, phone }));
        
        const { statusCode, data, message } = await apiSocios.Update({ id, name, age, email, phone });

        io.to(socket.id).emit('res:socios:update', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});

socket.on('req:socios:findOne', async ({ id }) => {

    try {

        console.log('req:socios:findOne', ({ id }));
        
        const { statusCode, data, message } = await apiSocios.FindOne({ id });

        io.to(socket.id).emit('res:socios:findOne', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});

socket.on('req:socios:view', async ({ enable }) => {

    try {

        console.log('req:socios:view', ({ enable }));
        
        const { statusCode, data, message } = await apiSocios.View({ enable });

        io.to(socket.id).emit('res:socios:view', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});

socket.on('req:socios:enable', async ({ id }) => {

    try {

        console.log('req:socios:enable', ({ id }));
        
        const { statusCode, data, message } = await apiSocios.Enable({ id });

        io.to(socket.id).emit('res:socios:enable', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});

socket.on('req:socios:disable', async ({ id }) => {

    try {

        console.log('req:socios:disable', ({ id }));
        
        const { statusCode, data, message } = await apiSocios.Disable({ id });

        io.to(socket.id).emit('res:socios:disable', { statusCode, data, message });
        
    } catch (error) {

        console.log(error);
        
    }
});
```