const bull = require('bull');

const nameApi = require('./package.json');

const name = nameApi.name.replace('api-','');

//console.log(nameApi.name);
//console.log(name);

/*
const { redis, name } = require('../microservice/src/setting');
No es viable ya que al empaquetar no se encotrara siempre con la misma ruta
al utilizarlo en otros micro
*/

const redis = { host: 'localhost', port: 6379 };

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull(`${name}:create`, opts);
const queueDelete = bull(`${name}:delete`, opts);
const queueUpdate = bull(`${name}:update`, opts);
const queueFindOne = bull(`${name}:findOne`, opts);
const queueView = bull(`${name}:view`, opts);
const queueEnable = bull(`${name}:enable`, opts);
const queueDisable = bull(`${name}:disable`, opts);

async function Create({ name, phone }) {

    try {
        const job = await queueCreate.add({ name, phone });

        console.log(nameApi.name + ': se agrega cola para CREAR con identificador ' + job.id );

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Delete({ id }) {

    try {
        const job = await queueDelete.add({ id });

        console.log(nameApi.name + ': se agrega cola para DELETE con identificador ' + job.id );

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Update({ id, name, age, email, phone }) {

    try {
        const job = await queueUpdate.add({ id, name, age, email, phone });

        console.log(nameApi.name + ': se agrega cola para UPDATE con identificador ' + job.id );

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function FindOne({ id }) {

    try {
        const job = await queueFindOne.add({ id });

        const { statusCode, data, message } = await job.finished();

        console.log(nameApi.name + ': se agrega cola para FINDONE con identificador ' + job.id );

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function View({ enable }) {

    try {
        const job = await queueView.add({ enable });

        console.log(nameApi.name + ': se agrega cola para VIEW con identificador ' + job.id );

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Enable({ id }) {

    try {
        const job = await queueEnable.add({ id });

        console.log(nameApi.name + ': se agrega cola para ENABLE con identificador ' + job.id );

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Disable({ id }) {

    try {
        const job = await queueDisable.add({ id });

        console.log(nameApi.name + ': se agrega cola para DISABLE con identificador ' + job.id );

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

console.log(nameApi.name + ' listo...');

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable };