const { Model } = require('../models/index');

async function Create({ name, phone }) {

    try {
        let instance = await Model.create(
            { name, phone },
            { fields: ['name', 'phone'] }
        );

        return { statusCode: 200, data: instance.toJSON() };

    } catch (error) {

        console.log({ step: 'controllers Create', error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
};

async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where });

        return { statusCode: 200, data: "Partner removed successfully" };
    } catch (error) {
        console.log({ step: 'controller Delete', error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

async function Update({ id, name, age, email, phone }) {
    try {
        let instance = await Model.update(
            { id, name, age, email, phone },
            { where: { id }, returning: true }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() };

    } catch (error) {
        console.log({ step: 'controller Update', error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

async function FindOne({ where = {} }) {
    try {
        let instance = await Model.findOne({ where });

        if (instance) return { statusCode: 200, data: instance.toJSON() };

        else return { statusCode: 400, message: "The partner you are looking for does not exist." };

    } catch (error) {
        console.log({ step: 'controller FindOne', error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

async function View({ where = {} }) {
    try {
        let instances = await Model.findAll({ where });
        return { statusCode: 200, data: instances };
    } catch (error) {
        console.log({ step: 'controller View', error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

async function Enable({ id }) {
    try {
        let instance = await Model.update(
            { enable: true },
            { where: { id }, returning: true }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() };

    } catch (error) {
        console.log({ step: 'controller Enable', error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

async function Disable({ id }) {
    try {
        let instance = await Model.update(
            { enable: false },
            { where: { id }, returning: true }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() };

    } catch (error) {
        console.log({ step: 'controller Disable', error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable };